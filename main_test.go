package main

import (
	"log"
	"testing"
)

type testCase struct {
	a, b, sum int
}

func TestSum(t *testing.T) {
	cases := []testCase{
		{
			1, 2, 3,
		},
		{
			5, 5, 10,
		},
	}

	for _, testCase := range cases {
		if testCase.sum != testCase.a+testCase.b {
			log.Fatalf("%d + %d != %d", testCase.a, testCase.b, testCase.sum)
		}
	}
}
