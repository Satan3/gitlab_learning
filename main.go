package main

import (
	"fmt"
	bot2 "k8sSample/app/bot"
	"log"
	"net/http"
	"os"
)

func main() {
	var err error
	port := os.Getenv("PORT")

	if port == "" {
		log.Fatal("Please fill the port value")
	}

	bot, err := bot2.New(os.Getenv("TLG_BOT_API_KEY"), os.Getenv("GIF_API_KEY"))
	if err != nil {
		log.Fatal("Cannot create TelegramBot:", err)
	}

	go bot.ProcessUpdates()

	http.HandleFunc("/", index)
	http.HandleFunc("/test", test)

	fmt.Println("Server starting...")

	err = http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
	if err != nil {
		log.Fatal(err)
	}
}

func index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Main API Page")
}

func test(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Vasya")
}

func Sum(a, b int) int {
	return a + b
}
