FROM golang:alpine

RUN go clean --modcache
RUN mkdir /app

COPY .. /app

WORKDIR /app

RUN go mod download

RUN go build -o main .

CMD ["/app/main"]