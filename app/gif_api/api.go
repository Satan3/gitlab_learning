package gif_api

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"os"
)

type Api struct {
	key string
}

type Images []Image

type Image struct {
	URL string `json:"url"`
}

type GifResponse struct {
	Data Images `json:"data"`
}

func New(key string) *Api {
	return &Api{key: key}
}

func (api *Api) GetGIF(query string) (string, error) {
	var response GifResponse

	offset := rand.Intn(1000-0) + 0

	url := fmt.Sprintf(
		"https://api.giphy.com/v1/gifs/search?api_key=%s&q=%s&limit=1&offset=%d&lang=ru",
		os.Getenv("GIF_API_KEY"),
		query,
		offset,
	)
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&response)
	if err != nil {
		return "", err
	}

	if len(response.Data) == 0 {
		return "Не удалось найти подходящую гифку", nil
	}

	return response.Data[0].URL, nil
}
