package bot

import (
	"fmt"
	tg "github.com/Syfaro/telegram-bot-api"
	"k8sSample/app/gif_api"
)

type Bot struct {
	Bot *tg.BotAPI
	API *gif_api.Api
}

func New(botApiKey, gifApiKey string) (*Bot, error) {
	bot, err := tg.NewBotAPI(botApiKey)
	if err != nil {
		return nil, err
	}
	return &Bot{bot, gif_api.New(gifApiKey)}, nil
}

func (bot *Bot) ProcessUpdates() {
	u := tg.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.Bot.GetUpdatesChan(u)
	if err != nil {
		fmt.Println(err)
		return
	}

	for update := range updates {
		if update.Message == nil {
			continue
		}

		text, err := bot.API.GetGIF(update.Message.Text)
		if err != nil {
			text = fmt.Sprintf("Ошибка получения картинки: %s", err.Error())
		}

		msg := createMessage(update.Message.Chat.ID, text)
		_, err = bot.Bot.Send(msg)
		if err != nil {
			fmt.Println("Ошибка отправки сообщения", err)
		}
	}
}

func createMessage(chatId int64, text string) tg.MessageConfig {
	return tg.NewMessage(chatId, text)
}
